/* src/command.rs: sub-command definitions & handlers
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use clap::Subcommand;
use miette::{miette, Result};
use rpassword::prompt_password;
use sqlx::SqlitePool;

use crate::{
    api,
    db::{subscription, Credential},
    telegram, Settings,
};

#[derive(Debug, Subcommand)]
pub enum Command {
    /// Authenticate broadband user and record API access keys
    Login {
        /// Broadband portal username
        username: String,
    },

    /// Obtain usage data from broadband portal and store them in database
    GetUsage,

    /// Query & process pending Telegram queue
    ProcessQueue,
}

impl Command {
    pub async fn handle(&self, settings: &Settings, db_pool: &SqlitePool) -> Result<()> {
        match self {
            Self::Login { username } => {
                let password = prompt_password("Password: ").map_err(|e| {
                    miette!(code = "bbud::login", "{}", e)
                        .wrap_err("Reading password from user input")
                })?;

                let client_id = api::get_client_id(&settings.api)?;
                let token = api::get_token(&settings.api, username, &password, &client_id)?;
                let credential = Credential::new(client_id, token);

                credential.save(db_pool).await?;

                eprintln!("New credentials successfully updated.")
            }

            Self::GetUsage => {
                let subscriber_list = subscription::get_all(db_pool).await?;
                let credentials = Credential::get(&db_pool).await?;
                let days_to_expire = settings.api.token_ttl
                    - (chrono::offset::Utc::now() - credentials.created).num_days() as u8;
                let mut usage = api::get_usage(&settings, &credentials)?;

                usage.save(&db_pool).await?;

                let claim = api::get_claim(&settings, &credentials)?;

                if 0.0 < claim.volume {
                    api::apply_claim(&settings, &credentials)?;
                }

                for subscriber in subscriber_list {
                    telegram::send(
                        &settings.telegram,
                        &format!("{}", subscriber.id),
                        &usage.into_message_text(),
                    )?;

                    if 0.0 < claim.volume {
                        telegram::send(
                            &settings.telegram,
                            &format!("{}", subscriber.id),
                            &claim.into_message_text(),
                        )?;
                    }
                }

                if settings.api.token_warning_threshold >= days_to_expire {
                    let warning_message = format!(
                        r#"
⚠️  Broadband portal access token will expire in {days_to_expire} day{:1}\.

Renew token by running *bbud login \<username\>* on the server\.
"#,
                        if 1 < days_to_expire { "s" } else { "" },
                    );
                    telegram::send(
                        &settings.telegram,
                        &settings.telegram.admin_id,
                        &warning_message,
                    )?;
                }
            }

            Self::ProcessQueue => {
                let last_offset = subscription::get_offset(db_pool).await;

                let update_list = telegram::get(&settings.telegram, last_offset + 1)?;

                for update in update_list {
                    match update.message.text.to_lowercase().trim() {
                        "/subscribe" | "/start" => match update.message.from.save(db_pool).await {
                            Ok(true) => {
                                let result = telegram::send(
                                    &settings.telegram,
                                    &format!("{}", update.message.from.id),
                                    "😃 You will recieve usage notificatons from now on\\.\n\n_I will send you the last usage summary from my archive shortly\\._",
                                );

                                if result.is_err() {
                                    eprintln!(
                                        "Acknowledging new /subscribe of {}\n\n{:#?}",
                                        update.message.from.id, result
                                    );
                                }

                                if let Ok(usage) = api::Usage::get(db_pool).await {
                                    let message = format!(
                                        "{}\n_recorded at {}_",
                                        usage.into_message_text(),
                                        usage.created.format("%l\\:%M%P\\, %B %d %Y \\(UTC\\)")
                                    );

                                    let result = telegram::send(
                                        &settings.telegram,
                                        &format!("{}", update.message.from.id),
                                        &message,
                                    );

                                    if result.is_err() {
                                        eprintln!(
                                            "Sending usage to new subscriber {}\n\n{:#?}",
                                            update.message.from.id, result
                                        );
                                    }
                                }
                            }
                            Ok(false) => {
                                let result = telegram::send(
                                    &settings.telegram,
                                    &format!("{}", update.message.from.id),
                                    "🤪 you are already subscribed\\.",
                                );

                                if result.is_err() {
                                    eprintln!(
                                        "Acknowledging repeated /subscribe of {}\n\n{:#?}",
                                        update.message.from.id, result
                                    );
                                }
                            }
                            Err(e) => {
                                let _ = telegram::send(
                                &settings.telegram,
                                &settings.telegram.admin_id,
                                &format!("🛑 Error processing__//subscribe__\n\n__Subscriber__\n```{:#?}```\n---\n\n__Error__\n```{:#?}```\n\n", update.message.from, e)
                                );
                            }
                        },
                        "/unsubscribe" => match update.message.from.remove(db_pool).await {
                            Ok(_) => {
                                let result = telegram::send(
                                    &settings.telegram,
                                    &format!("{}", update.message.from.id),
                                    "🤫 You no longer receive usage notifications\\.\n\n_I'll be quiet until you subscribe again_\\, *and you will\\! 😈*",
                                );

                                if result.is_err() {
                                    eprintln!(
                                        "Acknowledging /unsubscribe to {}\n\n{:#?}",
                                        update.message.from.id, result
                                    );
                                }
                            }
                            Err(e) => {
                                let _ = telegram::send(
                                &settings.telegram,
                                &settings.telegram.admin_id,
                                &format!("🛑 Error processing__//unsubscribe__\n\n__Subscriber__\n```{:#?}```\n---\n\n__Error__\n```{:#?}```\n\n", update.message.from, e)
                                );
                            }
                        },

                        // Notifications that are not messages or commands
                        "" => {}
                        _ => {
                            let _ = telegram::send(
                                &settings.telegram,
                                &format!("{}", update.message.from.id),
                                &format!("😕 I don't know how to \"_{}_\"", update.message.text),
                            );
                        }
                    }

                    subscription::save_offset(db_pool, update.update_id).await?;
                }
            }
        }

        Ok(())
    }
}
