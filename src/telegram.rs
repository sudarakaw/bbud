/* src/telegram.rs: Telegram bot API wrapper
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use miette::{miette, Result};
use serde::Deserialize;
use url::Url;

use crate::{db::subscription::Subscriber, settings::TelegramSettings};

const TELEGRAM_API_URL: &str = "https://api.telegram.org/bot";

#[derive(Debug, Deserialize)]
pub struct Update {
    pub update_id: u32,
    pub message: Message,
}

#[derive(Debug, Deserialize)]
pub struct Message {
    #[serde(default)]
    pub text: String,

    pub from: Subscriber,
}

pub fn send(settings: &TelegramSettings, recepient: &str, message: &str) -> Result<()> {
    let url = Url::parse(&format!("{}{}/sendMessage", TELEGRAM_API_URL, settings.key)).unwrap();

    ureq::request_url("POST", &url)
        .send_json(ureq::json!(
            { "chat_id": recepient
            , "parse_mode": "MarkdownV2"
            , "text": message
            }
        ))
        .map_err(|e| {
            let message = match e {
                ureq::Error::Status(code, resp) => {
                    format!("ERROR {}: {:?}", code, resp.into_string())
                }
                ureq::Error::Transport(t) => {
                    format!("Network error: {}", t)
                }
            };

            miette!(code = "bbud::telegram::send", "{}", message)
                .wrap_err("Sending telegram message")
        })?;

    Ok(())
}

pub fn get(settings: &TelegramSettings, offset: u32) -> Result<Vec<Update>> {
    let url = Url::parse(&format!("{}{}/getUpdates", TELEGRAM_API_URL, settings.key)).unwrap();

    #[derive(Debug, Deserialize, Default)]
    struct Body {
        ok: bool,
        result: Vec<Update>,
    }

    ureq::request_url("GET", &url)
        .send_form(&[("offset", &format!("{}", offset))])
        .map_err(|e| {
            let message = match e {
                ureq::Error::Status(code, resp) => {
                    format!("ERROR {}: {:?}", code, resp.into_string())
                }
                ureq::Error::Transport(t) => {
                    format!("Network error: {}", t)
                }
            };

            miette!(code = "bbud::telegram::get", "{}", message)
                .wrap_err("Getting telegram message queue")
        })?
        .into_json::<Body>()
        .map_err(|e| {
            miette!(code = "bbud::telegram::get", "{}", e)
                .wrap_err("Decoding telegram message queue")
        })
        .and_then(|body| {
            if body.ok {
                Ok(body.result)
            } else {
                Err(miette!(
                    code = "bbud::telegram::get",
                    "Telegram API call returned an error"
                ))
            }
        })
}
