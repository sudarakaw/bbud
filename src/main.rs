/* src/main.rs: main entry point for the executable program.
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use std::path::PathBuf;

use bbud::{db, Command, Settings, DEFAULT_SETTINGS_FILE};
use clap::Parser;
use dotenvy::dotenv;
use miette::Result;

#[derive(Parser, Debug)]
#[command(version, author, about)]
struct Args {
    #[arg(env("BBUD_CONFIG"), short, long, value_name = "FILE", default_value_os_t = DEFAULT_SETTINGS_FILE.into())]
    config: PathBuf,

    #[command(subcommand)]
    cmd: Command,
}

#[tokio::main]
async fn main() -> Result<()> {
    dotenv().ok();

    let args = Args::parse();
    let settings = Settings::load(&args.config)?;
    let db_pool = db::connect(&settings.db).await?;

    args.cmd.handle(&settings, &db_pool).await?;

    Ok(())
}
