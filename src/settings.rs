/* src/config.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! Load configuration from filesystem and/or environment

use std::path::{Path, PathBuf};

use config::{Config, Environment, File, FileFormat};
use miette::{miette, Result};
use serde::Deserialize;
use url::Url;

use crate::package;

#[derive(Debug, Deserialize, Default)]
#[serde(default)]
pub struct Settings {
    pub api: ApiSettings,
    pub db: DbSettings,
    pub telegram: TelegramSettings,
}

impl Settings {
    pub fn load(filename: &Path) -> Result<Self> {
        let settings = Config::builder()
            .add_source(
                File::with_name(filename.to_str().unwrap())
                    .required(false)
                    .format(FileFormat::Ini),
            )
            .add_source(Environment::with_prefix(package::NAME).separator("_"))
            .build()
            .map_err(|e| miette!(code = "bbud::config::read", help = "Make sure configuration file exists and readable by the user bbud running as.", "{}", e).wrap_err("Loading configuration"))?
            .try_deserialize::<Self>()
            .map_err(|e| miette!(code = "bbud::config::decode", help = "Make sure configuration file content is in valid INI format.\nOptionally, you can remove the configuration file and let file with default configuration to be created.", "{}", e).wrap_err("Decode loaded configuration"))?;

        Ok(settings)
    }
}

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct ApiSettings {
    #[serde(rename = "baseurl")]
    pub base_url: Url,
    #[serde(rename = "clientidurl")]
    pub client_id_url: Url,
    #[serde(rename = "subscriberid")]
    pub subscriber_id: String,
    pub token_ttl: u8,
    pub token_warning_threshold: u8,
}

impl Default for ApiSettings {
    fn default() -> Self {
        Self {
            base_url: Url::parse("http://localhost/").unwrap(),
            client_id_url: Url::parse("http://localhost/client_id").unwrap(),
            subscriber_id: "".to_string(),
            token_ttl: 14,
            token_warning_threshold: 2,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct DbSettings {
    pub path: PathBuf,
}

impl Default for DbSettings {
    fn default() -> Self {
        Self {
            path: format!("/var/lib/{}/database", package::NAME).into(),
        }
    }
}

#[derive(Debug, Deserialize, Default)]
pub struct TelegramSettings {
    pub key: String,
    #[serde(rename = "adminid")]
    pub admin_id: String,
}
