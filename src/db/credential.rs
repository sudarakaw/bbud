/* src/db/credential.rs: Credential data structures & database functions
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use chrono::{DateTime, Utc};
use miette::{miette, Result};
use sqlx::{Error, SqlitePool};

use super::{commit_transaction, start_transaction};

#[derive(Debug, sqlx::Type, Clone)]
pub struct Credential {
    pub client_id: String,
    pub token: String,
    pub created: DateTime<Utc>,
}

impl Credential {
    pub fn new(client_id: String, token: String) -> Self {
        Self {
            client_id,
            token,
            created: DateTime::default(),
        }
    }

    pub async fn get(db_pool: &SqlitePool) -> Result<Self> {
        sqlx::query_as!(
            Self,
            r#"SELECT `client_id`
                    , `token`
                    , `created` as "created!: DateTime<Utc>"
               FROM `credential`"#
        )
        .fetch_one(db_pool)
        .await
        .map_err(|e| {
            let error = match e {
                Error::RowNotFound => {
                    miette!(
                        code = "bbud::credential::get",
                        help = "use \"login\" command to obtain new credentials.",
                        "No credentials found."
                    )
                }
                _ => miette!(code = "bbud::credential::get", "{}", e),
            };

            error.wrap_err("Getting stored credentials")
        })
    }

    pub async fn save(&self, db_pool: &SqlitePool) -> Result<()> {
        let transaction = start_transaction(db_pool).await?;

        sqlx::query!(r#"DELETE FROM `credential`"#)
            .execute(db_pool)
            .await
            .map_err(|e| {
                miette!(code = "bbud::credential::save", "{}", e)
                    .wrap_err("Removing stored credentials")
            })?;

        sqlx::query!(
            r#"
            INSERT INTO `credential`
                ( `client_id`
                , `token`
                )
            VALUES ($1, $2)
            "#,
            self.client_id,
            self.token
        )
        .execute(db_pool)
        .await
        .map_err(|e| {
            miette!(code = "bbud::credential::save", "{}", e).wrap_err("Store new credentials")
        })?;

        commit_transaction(transaction).await?;

        Ok(())
    }
}
