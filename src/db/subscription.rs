/* src/db/subscription.rs: Telegram subscription database functions
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use miette::{miette, Result};
use serde::Deserialize;
use sqlx::{error::ErrorKind, Error, SqlitePool};

use super::{commit_transaction, start_transaction};

#[derive(Debug, Deserialize)]
pub struct Subscriber {
    pub id: u32,
    first_name: String,
}

impl Subscriber {
    pub async fn remove(&self, db_pool: &SqlitePool) -> Result<(), sqlx::Error> {
        sqlx::query!(
            r#"
            DELETE FROM `subscription`
            WHERE `id` = $1
            "#,
            self.id,
        )
        .execute(db_pool)
        .await
        .map(|_| ())
    }

    pub async fn save(&self, db_pool: &SqlitePool) -> Result<bool, sqlx::Error> {
        let result = sqlx::query!(
            r#"
            INSERT INTO `subscription`
                ( `id`
                , `first_name`
                )
            VALUES ($1, $2)
            "#,
            self.id,
            self.first_name
        )
        .execute(db_pool)
        .await;

        match result {
            Ok(_) => Ok(true),
            Err(e) => {
                if let Error::Database(ref db_error) = e {
                    if ErrorKind::UniqueViolation == db_error.kind() {
                        // Subscribe id already exists.
                        // This is an ok situation as user does not need to
                        // subscribe again

                        return Ok(false);
                    }
                }

                Err(e)
            }
        }
    }
}

pub async fn get_offset(db_pool: &SqlitePool) -> u32 {
    sqlx::query!(r#"SELECT `offset` as "offset!: u32" FROM `tg_offset`"#)
        .fetch_one(db_pool)
        .await
        .map(|r| r.offset)
        .unwrap_or(0)
}

pub async fn save_offset(db_pool: &SqlitePool, offset: u32) -> Result<()> {
    let transaction = start_transaction(db_pool).await?;

    sqlx::query!(r#"DELETE FROM `tg_offset`"#)
        .execute(db_pool)
        .await
        .map_err(|e| {
            miette!(code = "bbud::subscription::offset::save", "{}", e)
                .wrap_err("Removing stored offset")
        })?;

    sqlx::query!(
        r#"
        INSERT INTO `tg_offset`
            ( `offset`
            )
        VALUES ($1)
        "#,
        offset
    )
    .execute(db_pool)
    .await
    .map_err(|e| {
        miette!(code = "bbud::subscription::offset::save", "{}", e).wrap_err("Store new offset")
    })?;

    commit_transaction(transaction).await?;

    Ok(())
}

pub async fn get_all(db_pool: &SqlitePool) -> Result<Vec<Subscriber>> {
    sqlx::query_as!(
        Subscriber,
        r#"
        SELECT `id` as "id!: u32"
                , `first_name`
        FROM `subscription`
        "#
    )
    .fetch_all(db_pool)
    .await
    .map_err(|e| {
        miette!(code = "bbud::subscription::get_all", "{}", e)
            .wrap_err("Getting subscriber list from database")
    })
}
