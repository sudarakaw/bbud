/* src/db.rs: Database access module
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod credential;
pub use credential::Credential;

pub mod subscription;

use miette::{miette, Result};
use sqlx::{Sqlite, SqlitePool, Transaction};

use crate::settings::DbSettings;

pub async fn connect(settings: &DbSettings) -> Result<SqlitePool> {
    let db_pool =
        SqlitePool::connect_lazy(&format!("sqlite://{}?mode=rwc", settings.path.display()))
            .map_err(|e| {
                miette!(code = "bbud::db", "{}", e).wrap_err(format!(
                    "Connecting to database {}",
                    settings.path.display()
                ))
            })?;

    sqlx::migrate!()
        .run(&db_pool)
        .await
        .map_err(|e| miette!(code = "bbud::db", "{}", e).wrap_err("Running database migrations"))?;

    Ok(db_pool)
}

async fn start_transaction(db_pool: &SqlitePool) -> Result<Transaction<'_, Sqlite>> {
    db_pool
        .begin()
        .await
        .map_err(|e| miette!(code = "bbud::db::save", "{}", e).wrap_err("Starting new transaction"))
}

async fn commit_transaction(transaction: Transaction<'_, Sqlite>) -> Result<()> {
    transaction
        .commit()
        .await
        .map_err(|e| miette!(code = "bbud::db::save", "{}", e).wrap_err("Commiting transaction"))
}
