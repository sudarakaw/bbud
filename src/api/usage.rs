/* src/api/usage.rs: Usage API module
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

use chrono::{Datelike, Duration, Local, NaiveDate, NaiveDateTime, Timelike};
use miette::{miette, Result};
use serde::Deserialize;
use sqlx::{Error, SqlitePool};

use crate::{api::ApiResult, db::Credential, package, telegram, Settings};

const SECONDS_PER_DAY: f32 = 60.0 * 60.0 * 24.0;

#[derive(Debug, Default)]
pub struct Usage {
    pub bonus: Data,
    pub standard: Data,
    pub addon: Data,
    cached: bool,
    pub created: NaiveDateTime,
}

impl Usage {
    pub async fn get(db_pool: &SqlitePool) -> Result<Self> {
        sqlx::query!(
            r#"
            SELECT `bonus_total` as "bonus_total!: f32"
                 , `bonus_used` as "bonus_used!: f32"
                 , `bonus_remaining` as "bonus_remaining!: f32"
                 , `bonus_unit`
                 , `standard_total` as "standard_total!: f32"
                 , `standard_used` as "standard_used!: f32"
                 , `standard_remaining` as "standard_remaining!: f32"
                 , `standard_unit`
                 , `addon_total` as "addon_total!: f32"
                 , `addon_used` as "addon_used!: f32"
                 , `addon_remaining` as "addon_remaining!: f32"
                 , `addon_unit`
                 , `created` as "created!: NaiveDateTime"
            FROM `usage`
            ORDER BY `created` DESC
            LIMIT 1
            "#
        )
        .fetch_one(db_pool)
        .await
        .map(|r| {
            let mut addon = Data {
                total: r.addon_total,
                used: r.addon_used,
                unit: r.addon_unit.clone(),
                ..Default::default()
            };

            addon.remaining = addon.total - addon.used;

            let mut bonus = Data {
                total: r.bonus_total,
                used: r.bonus_used,
                unit: r.bonus_unit.clone(),
                ..Default::default()
            };

            bonus.remaining = bonus.total - bonus.used;

            let standard = Data {
                total: r.standard_total,
                used: r.standard_used,
                remaining: r.standard_remaining,
                unit: r.standard_unit.clone(),
            };

            Self {
                bonus,
                standard,
                addon,
                cached: true,
                created: r.created,
            }
        })
        .map_err(|e| {
            let error = match e {
                Error::RowNotFound => {
                    miette!(
                        code = "bbud::usage::get",
                        help = "use \"get-usage\" command to obtain latest usage data",
                        "No usage data found."
                    )
                }
                _ => miette!(code = "bbud::usage::get", "{}", e),
            };

            error.wrap_err("Getting stored usage")
        })
    }

    pub async fn save(&mut self, db_pool: &SqlitePool) -> Result<()> {
        if !self.cached {
            sqlx::query!(
                r#"
                INSERT INTO `usage`
                    ( `bonus_total`
                    , `bonus_used`
                    , `bonus_remaining`
                    , `bonus_unit`
                    , `standard_total`
                    , `standard_used`
                    , `standard_remaining`
                    , `standard_unit`
                    , `addon_total`
                    , `addon_used`
                    , `addon_remaining`
                    , `addon_unit`
                    )
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
                "#,
                self.bonus.total,
                self.bonus.used,
                self.bonus.remaining,
                self.bonus.unit,
                self.standard.total,
                self.standard.used,
                self.standard.remaining,
                self.standard.unit,
                self.addon.total,
                self.addon.used,
                self.addon.remaining,
                self.addon.unit,
            )
            .execute(db_pool)
            .await
            .map_err(|e| {
                miette!(code = "bbud::usage::save", "{}", e).wrap_err("Store new credentials")
            })?;

            self.cached = true;
        }

        Ok(())
    }

    pub fn into_message_text(&self) -> String {
        let Usage {
            bonus,
            standard,
            addon,
            ..
        } = self;
        let now = Local::now();
        let first_of_the_month = now
            .naive_local()
            .with_day(1)
            .and_then(|d| d.with_hour(0))
            .and_then(|d| d.with_minute(0))
            .and_then(|d| d.with_second(0))
            .and_then(|d| d.with_nanosecond(0))
            .unwrap();
        let seconds_elapsed = (now.naive_local() - first_of_the_month).num_seconds();

        let last_date = NaiveDate::from_ymd_opt(now.year(), now.month() + 1, 1)
            .or_else(|| NaiveDate::from_ymd_opt(now.year() + 1, 1, 1))
            .and_then(|d| d.pred_opt())
            .unwrap();
        let mut days_remaining = (last_date - now.date_naive()).num_days() + 1;

        if 1 > days_remaining {
            days_remaining = 1;
        }

        let addon_text = if 0.0 < addon.total && 0.0 < addon.remaining {
            format!(
                r#"
`Addon Remaining : {:6.2}` _of {:6.2} {unit}_
"#,
                addon.remaining,
                addon.total,
                unit = addon.unit,
            )
            .replace(".", "\\.")
        } else {
            "".to_string()
        };

        if 0.0 < bonus.remaining {
            let seconds_can_last = if 0.0 < bonus.used {
                seconds_elapsed as f32 / bonus.used * bonus.total
            } else {
                1.0
            };

            let last_until = Duration::try_seconds(seconds_can_last as i64)
                .map(|duration| {
                    NaiveDate::from_ymd_opt(now.year(), now.month(), 1).unwrap() + duration
                })
                .map(|date| date.format("%B %d, %Y").to_string())
                .unwrap_or("n/a".to_string());

            format!(
                r#"
__Usage Summary \- _*Bonus* {} {unit}_ __

`Used            : {:6.2} {unit}`
`Average Usage   : {:6.2} {unit} / day`

`Remaining       : {:6.2} {unit}`
`Est. Exhaust    : {}`

`Next {days_remaining:2.0} day{:1}    : {:6.2} {unit} / day`
{addon_text}
_generated by {pkg_name} v{pkg_ver}_
"#,
                bonus.total.to_string().replace(".", "\\."),
                (bonus.used + addon.used),
                (bonus.used + addon.used) / (seconds_elapsed as f32 / SECONDS_PER_DAY),
                bonus.remaining,
                last_until,
                if 1 < days_remaining { "s" } else { "" },
                (bonus.remaining + standard.remaining) / days_remaining as f32,
                unit = bonus.unit,
                pkg_name = package::NAME,
                pkg_ver = package::VERSION.replace(".", "\\."),
            )
        } else {
            format!(
                r#"
__Usage Summary \- _*Standard* {} {unit}_ __

`Used            : {:6.2} {unit}`
`Average Usage   : {:6.2} {unit} / day`

`Remaining       : {:6.2} {unit}`
`Next {days_remaining:2.0} day{:1}    : {:6.2} {unit} / day`
{addon_text}
_generated by {pkg_name} v{pkg_ver}_
"#,
                standard.total.to_string().replace(".", "\\."),
                (standard.used + bonus.used + addon.used),
                (standard.used + bonus.used + addon.used)
                    / (seconds_elapsed as f32 / SECONDS_PER_DAY),
                standard.remaining,
                if 1 < days_remaining { "s" } else { "" },
                standard.remaining / days_remaining as f32,
                unit = standard.unit,
                pkg_name = package::NAME,
                pkg_ver = package::VERSION.replace(".", "\\."),
            )
        }
    }
}

impl<'de> Deserialize<'de> for Usage {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Raw {
            bonus_data_summary: Data,
            vas_data_summary: Option<Data>,
            my_package_info: Info,
        }

        #[derive(Deserialize)]
        struct Info {
            #[serde(rename = "usageDetails")]
            usage_details: Vec<Pkg>,
        }

        #[derive(Deserialize)]
        struct Pkg {
            name: String,
            limit: String,
            used: String,
            remaining: String,
            volume_unit: String,
        }

        let raw = Raw::deserialize(deserializer)?;
        let standard = raw
            .my_package_info
            .usage_details
            .iter()
            .find(|p| "Standard" == p.name)
            .map(|p| Data {
                total: p.limit.parse().unwrap_or_default(),
                used: p.used.parse().unwrap_or_default(),
                remaining: p.remaining.parse().unwrap_or_default(),
                unit: p.volume_unit.clone(),
            })
            .unwrap_or_default();

        Ok(Usage {
            bonus: raw.bonus_data_summary,
            standard,
            addon: raw.vas_data_summary.unwrap_or_default(),
            ..Default::default()
        })
    }
}

#[derive(Debug, Default)]
pub struct Data {
    pub total: f32,
    pub used: f32,
    pub remaining: f32,
    pub unit: String,
}

impl<'de> Deserialize<'de> for Data {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Debug, Deserialize)]
        struct Raw {
            limit: String,
            used: String,
            volume_unit: String,
        }

        let raw = Raw::deserialize(deserializer)?;

        Ok(Data {
            total: raw.limit.parse().unwrap_or_default(),
            used: raw.used.parse().unwrap_or_default(),
            unit: raw.volume_unit.clone(),
            ..Default::default()
        })
    }
}

pub fn get_usage(settings: &Settings, credentials: &Credential) -> Result<Usage> {
    let error_code = "bbud::usage";

    if settings.api.subscriber_id.trim().is_empty() {
        return Err(
            miette!(code = error_code, "{}", "Subscribe ID was not provided.")
                .wrap_err("Requesting usage data from portal"),
        );
    }

    let mut url = settings.api.base_url.join("BBVAS/UsageSummary").unwrap();

    url.set_query(Some(&format!(
        "subscriberID={}",
        settings.api.subscriber_id
    )));

    let result = ureq::request_url("GET", &url)
        .set("X-IBM-Client-Id", &credentials.client_id)
        .set("Authorization", &format!("bearer {}", credentials.token))
        .call()
        .map_err(|e| {
            let error = match e {
                ureq::Error::Status(401, _) => {
                    let _ = telegram::send(
                        &settings.telegram,
                        &settings.telegram.admin_id,
                        r#"
🛑 Broadband portal access token invalid\.

Please run *bbud login \<username\>* on the server\.
"#,
                    );

                    miette!(
                        code = error_code,
                        help =
                            "You can obtain new credentials by running \"bbud login <username>\"",
                        "{}",
                        "Invalid login credentials"
                    )
                }
                ureq::Error::Status(http_code, resp) => {
                    miette!(code = error_code, "ERROR {}: {:?}", http_code, resp)
                }
                ureq::Error::Transport(t) => miette!(code = error_code, "Network error: {}", t),
            };

            error.wrap_err("Requesting usage data from portal")
        })?
        .into_json::<ApiResult<Usage>>()
        .map_err(|e| {
            miette!(code = error_code, "{}", e).wrap_err("Decoding broadband usage response")
        })
        .map(|mut result| {
            // Calculate remaining amount of bonus data.
            if 0.0 == result.data.bonus.remaining {
                result.data.bonus.remaining = result.data.bonus.total - result.data.bonus.used;
            }

            // Calculate remaining amount of addon data.
            if 0.0 == result.data.addon.remaining {
                result.data.addon.remaining = result.data.addon.total - result.data.addon.used;
            }

            result
        })?;

    if !result.is_success {
        return Err(miette!(
            code = error_code,
            "{}",
            result.message.unwrap_or("".to_string())
        )
        .wrap_err("Requesting usage data from portal"));
    }

    Ok(result.data)
}
