/* src/api/mod.rs: main module for API calls
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

mod usage;
pub use usage::*;

mod bonus_claim;
pub use bonus_claim::*;

use miette::{miette, Result};
use serde::Deserialize;

use crate::ApiSettings;

#[derive(Deserialize, Default)]
struct ApiResult<T>
where
    T: Default,
{
    #[serde(rename = "isSuccess")]
    is_success: bool,

    #[serde(default, rename = "errorMessege")]
    message: Option<String>,

    #[serde(default, rename = "dataBundle")]
    data: T,
}

pub fn get_client_id(settings: &ApiSettings) -> Result<String> {
    ureq::request_url("GET", &settings.client_id_url)
        .call()
        .map_err(|e| {
            miette!(code = "bbud::login", "{}", e).wrap_err("Obtaining client ID file from server")
        })?
        .into_string()
        .and_then(|html| {
            let mut client_id_iter = html.split("X-IBM-Client-Id\"]=\"");

            client_id_iter.next();

            Ok(client_id_iter
                .collect::<Vec<_>>()
                .first()
                .or(Some(&""))
                .unwrap()
                .split('"')
                .collect::<Vec<_>>()
                .first()
                .or(Some(&""))
                .unwrap()
                .to_string())
        })
        .map_err(|e| {
            miette!(code = "bbud::login", "{}", e).wrap_err(format!(
                "Unable to locate client ID in file: {}",
                settings.client_id_url
            ))
        })
}

pub fn get_token(
    settings: &ApiSettings,
    username: &str,
    password: &str,
    client_id: &str,
) -> Result<String> {
    #[derive(Debug, Deserialize, Default)]
    struct Body {
        #[serde(default, rename = "accessToken")]
        access_token: String,

        #[serde(default, rename = "errorMessege")]
        message: String,
    }

    let body = ureq::request_url("POST", &settings.base_url.join("Account/Login").unwrap())
        .set("X-IBM-Client-Id", client_id)
        .send_form(&[
            ("username", username),
            ("password", password),
            ("channelID", "WEB"),
        ])
        .map_err(|e| {
            #[derive(Debug, Deserialize, Default)]
            struct ErrorMessage {
                message: String,
            }

            let message = match e {
                ureq::Error::Status(400, resp) => match resp.into_json::<ErrorMessage>() {
                    Ok(msg) => msg.message,
                    Err(e) => format!("{}", e),
                },
                ureq::Error::Status(401, _) => "Invalid login credentials".to_string(),
                ureq::Error::Status(code, resp) => {
                    format!("ERROR {}: {:?}", code, resp.into_string())
                }
                ureq::Error::Transport(t) => {
                    format!("Network error: {}", t)
                }
            };

            miette!(code = "bbud::login", "{}", message)
                .wrap_err("Sending credentials to broadband portal")
        })?
        .into_json::<Body>()
        .map_err(|e| {
            miette!(code = "bbud::login", "{}", e)
                .wrap_err("Decoding broadband portal login response")
        })
        .and_then(|body| {
            if body.message.is_empty() {
                Ok(body)
            } else {
                Err(miette!(code = "bbud::login", "{}", body.message))
            }
        })?;

    Ok(body.access_token)
}
