/* src/api/bonus_claim.rs: Get information about claimable bonus data
 *
 * Copyright 2024 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for
 * mote details.
 *
 */

use miette::{miette, Result};
use serde::Deserialize;

use crate::{api::ApiResult, db::Credential, Settings};

#[derive(Debug, Default)]
pub struct Claim {
    pub volume: f32,
    pub unit: String,
}

impl Claim {
    pub fn into_message_text(&self) -> String {
        let Claim { volume, unit } = self;
        let volume_str = format!("{:.2}", volume).replace(".", "\\.");

        format!(
            r#"
🎁 You got additional *{volume_str}{unit}* bonus data available\!
            "#,
        )
    }
}

impl<'de> Deserialize<'de> for Claim {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Debug, Deserialize)]
        struct Raw {
            #[serde(rename = "usageDetails")]
            usage: Vec<UsageDetails>,
        }

        #[derive(Debug, Deserialize)]
        struct UsageDetails {
            limit: f32,
            claim: Option<RawClaim>,
        }

        #[derive(Debug, Deserialize)]
        struct RawClaim {
            volume: String,
            volume_unit: String,
        }

        let claim = Raw::deserialize(deserializer)?
            .usage
            .pop()
            .and_then(
                |UsageDetails {
                     limit,
                     claim: opt_claim,
                 }| {
                    opt_claim.map(|oc| Claim {
                        volume: oc
                            .volume
                            .parse::<f32>()
                            .map(|v| v - limit)
                            .unwrap_or_default(),
                        unit: oc.volume_unit.clone(),
                    })
                },
            )
            .unwrap_or_default();

        Ok(claim)
    }
}

pub fn get_claim(settings: &Settings, credentials: &Credential) -> Result<Claim> {
    let error_code = "bbud::claim";

    if settings.api.subscriber_id.trim().is_empty() {
        return Err(
            miette!(code = error_code, "{}", "Subscribe ID was not provided.")
                .wrap_err("Requesting bonus claim data from portal"),
        );
    }

    let mut url = settings.api.base_url.join("BBVAS/BonusData").unwrap();

    url.set_query(Some(&format!(
        "subscriberID={}",
        settings.api.subscriber_id
    )));

    let result = ureq::request_url("GET", &url)
        .set("X-IBM-Client-Id", &credentials.client_id)
        .set("Authorization", &format!("bearer {}", credentials.token))
        .call()
        .map_err(|e| {
            let error = match e {
                ureq::Error::Status(http_code, resp) => {
                    miette!(code = error_code, "ERROR {}: {:?}", http_code, resp)
                }
                ureq::Error::Transport(t) => miette!(code = error_code, "Network error: {}", t),
            };

            error.wrap_err("Requesting bonus claim data from portal")
        })?
        .into_json::<ApiResult<Claim>>()
        .map_err(|e| {
            miette!(code = error_code, "{}", e).wrap_err("Decoding broadband bonus claim response")
        })?;

    if result.is_success {
        Ok(result.data)
    } else {
        Ok(Claim::default())
    }
}

pub fn apply_claim(settings: &Settings, credentials: &Credential) -> Result<()> {
    let error_code = "bbud::claim";

    if settings.api.subscriber_id.trim().is_empty() {
        return Err(
            miette!(code = error_code, "{}", "Subscribe ID was not provided.")
                .wrap_err("Upgrading bonus data in portal"),
        );
    }

    let mut url = settings.api.base_url.join("BBVAS/UpgradeLoyalty").unwrap();

    url.set_query(Some(&format!(
        "subscriberID={}",
        settings.api.subscriber_id
    )));

    ureq::request_url("PUT", &url)
        .set("X-IBM-Client-Id", &credentials.client_id)
        .set("Authorization", &format!("bearer {}", credentials.token))
        .call()
        .map_err(|e| {
            let error = match e {
                ureq::Error::Status(http_code, resp) => {
                    miette!(code = error_code, "ERROR {}: {:?}", http_code, resp)
                }
                ureq::Error::Transport(t) => miette!(code = error_code, "Network error: {}", t),
            };

            error.wrap_err("Upgrading bonus data in portal")
        })?;

    Ok(())
}
