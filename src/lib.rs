/* src/lib.rs: main module for the application
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify it
 * under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

mod command;
pub use command::Command;

mod api;

pub mod db;

mod package;
pub use package::*;

mod settings;
pub use settings::{ApiSettings, Settings};

pub mod telegram;
