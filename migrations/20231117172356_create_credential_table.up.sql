-- migrations/20231117172356_create_credential_table.up.sql
--
-- Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE IF NOT EXISTS `credential` (
          `client_id` VARCHAR(64) NOT NULL
        , `token` TEXT NOT NULL
        , `created` DATETIME DEFAULT CURRENT_TIMESTAMP
          );
