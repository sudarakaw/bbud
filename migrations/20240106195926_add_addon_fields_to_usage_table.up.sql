-- migrations/20240106195926_add_addon_fields_to_usage_table.up.sql
--
-- Copyright 2024 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for
-- mote details.
--
ALTER     TABLE `usage`
ADD       COLUMN `addon_total` DECIMAL(6, 2) NOT NULL DEFAULT 0;


ALTER     TABLE `usage`
ADD       COLUMN `addon_used` DECIMAL(6, 2) NOT NULL DEFAULT 0;


ALTER     TABLE `usage`
ADD       COLUMN `addon_remaining` DECIMAL(6, 2) NOT NULL DEFAULT 0;


ALTER     TABLE `usage`
ADD       COLUMN `addon_unit` VARCHAR(8) NOT NULL DEFAULT 'GB';
