-- migrations/20231125213343_create_usage_table.up.sql
--
-- Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE IF NOT EXISTS `usage` (
          `created` DATETIME DEFAULT CURRENT_TIMESTAMP
        , `bonus_total` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `bonus_used` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `bonus_remaining` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `bonus_unit` VARCHAR(8) NOT NULL DEFAULT 'GB'
        , `standard_total` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `standard_used` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `standard_remaining` DECIMAL(6, 2) NOT NULL DEFAULT 0
        , `standard_unit` VARCHAR(8) NOT NULL DEFAULT 'GB'
          );
