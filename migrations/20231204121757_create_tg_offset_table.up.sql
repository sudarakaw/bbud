-- migrations/20231204121757_create_tg_offset_table.up.sql
--
-- Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE IF NOT EXISTS `tg_offset` (
          `created` DATETIME DEFAULT CURRENT_TIMESTAMP
        , `offset` INTEGER NOT NULL DEFAULT 0 CHECK (`offset` >= 0)
          );
