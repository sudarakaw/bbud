-- migrations/20240106195926_add_addon_fields_to_usage_table.down.sql
--
-- Copyright 2024 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY;
-- This is free software, and you are welcome to redistribute it and/or modify
-- it under the terms of the BSD 2-clause License. See the LICENSE file for
-- mote details.
--
ALTER     TABLE `usage`
DROP      COLUMN `addon_total`;


ALTER     TABLE `usage`
DROP      COLUMN `addon_used`;


ALTER     TABLE `usage`
DROP      COLUMN `addon_remaining`;


ALTER     TABLE `usage`
DROP      COLUMN `addon_unit`;
